#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import sys

if sys.version_info.major < 3:
    raise SystemExit('python 3 later is required')

setup(
    name="sqlalchemy_session",
    version="1.1",
    install_requires=[
        "sqlalchemy"
    ],
    packages=['sqlalchemy_session'],
)
